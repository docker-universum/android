FROM universumstudios/android:sdk

### CONFIGURATION ==================================================================================

# Environment variables:
ENV ANDROID_API_LEVEL=28
ENV ANDROID_EMULATOR_NAME="test-${ANDROID_API_LEVEL}"
ENV ANDROID_EMULATOR_ABI=x86

# Installation arguments:
ARG ANDROID_EMULATOR_PACKAGE="system-images;android-${ANDROID_API_LEVEL};google_apis;${ANDROID_EMULATOR_ABI}"
	
### ANDROID ========================================================================================

## Platform components (update):
RUN set -o errexit -o nounset \
    && echo "Updating Android Platform API" \
	&& echo yes | sdkmanager "platforms;android-${ANDROID_API_LEVEL}" \
	\
	&& echo "Updating Android SDK Build tools" \
	&& echo yes | sdkmanager "build-tools;${ANDROID_API_LEVEL}.0.0" \
	&& echo yes | sdkmanager "build-tools;${ANDROID_API_LEVEL}.0.1" \
	&& echo yes | sdkmanager "build-tools;${ANDROID_API_LEVEL}.0.2" \
	&& echo yes | sdkmanager "build-tools;${ANDROID_API_LEVEL}.0.3"

## Emulator (update and create):
COPY emulator/ $ANDROID_HOME/emulator/
RUN set -o errexit -o nounset \
    && echo "Updating Android Emulator" \
    && echo yes | sdkmanager $ANDROID_EMULATOR_PACKAGE \
	\
	&& echo "Creating Android Emulator" \
	&& echo no | avdmanager --silent create avd --package $ANDROID_EMULATOR_PACKAGE --name $ANDROID_EMULATOR_NAME --force --abi google_apis/$ANDROID_EMULATOR_ABI

### VERIFICATION ===================================================================================

RUN set -o errexit -o nounset \	
    && echo "Testing Android Platform API configuration" \
	&& sdkmanager --list