Android SDK Image
===============

## Overview ##

_Docker_ images that contain components required for **building** of _Android_ projects.

## Tags ##

Below are listed tags for which are **available** their corresponding **images**.

- **[sdk]()**
- **[api-xx]()**

> To use for example image which provides **Android API 34** components use: `image: universumstudios/android:api-34`.

## Cloud ##

Images are available via **[Docker Cloud](https://hub.docker.com/repository/docker/universumstudios/android/general)**.

## [License](https://bitbucket.org/docker-universum/android/src/master/README.md) ##

**Copyright 2024 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.
