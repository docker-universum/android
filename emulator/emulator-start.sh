#!/usr/bin/env bash

emulatorName=${1}

if [ ! ${emulatorName} ]; then
	echo "Name of the emulator not specified or it is empty!"
    exit 1
fi

# Start the emulator and wait until it boots up:
emulator -avd ${emulatorName} -no-window -no-audio &
emulator-wait.sh ${emulatorName}

if [ $? == 1 ]; then
    # Failed to start emulator.
    exit 1
fi

adb shell input keyevent 82 &
