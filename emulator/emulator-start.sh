#!/usr/bin/env bash

EMULATOR_NAME=${1}
if [ ! ${EMULATOR_NAME} ]; then
	echo "Name of the emulator not specified or it is empty!"
    exit 1
fi

# Start the emulator and wait until it boots up:
emulator -avd ${EMULATOR_NAME} -no-window -no-audio &
emulator-wait.sh ${EMULATOR_NAME}
if [ $? == 1 ]; then
    # Failed to start emulator.
    exit 1
fi
adb shell input keyevent 82 &