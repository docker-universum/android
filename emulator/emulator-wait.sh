#!/usr/bin/env bash

emulatorName=${1}

if [ ! ${emulatorName} ]; then
	echo "Name of the emulator not specified or it is empty!"
  exit 1
fi

# States of the boot animation:
BOOT_ANIMATION_IDLE=""
BOOT_ANIMATION_STOPPED="stopped"
BOOT_ANIMATION_NOT_FOUND="device_not_found"
BOOT_ANIMATION_OFFLINE="device_offline"
BOOT_ANIMATION_RUNNING="running"

# Timeout (in seconds) to wait for emulator to boot up.
TIMEOUT=360

# Delay (in seconds) to wait until next check for boot animation's state is performed.
NEXT_STATE_CHECK_DELAY=5

set +e

function formatWaitDuration {
    duration=${1}
    formattedDuration=""
    
    if [[ ${duration} -lt 60 ]]; then
        formattedDuration="${duration} seconds"
    else
        (( durationSeconds=${duration} % 60 ))
        (( durationMinutes=${duration} / 60 ))

        formattedDuration="${durationMinutes} minutes ${durationSeconds} seconds"
    fi

    echo ${formattedDuration}
}

bootAnimationState="${BOOT_ANIMATION_IDLE}"
waitDuration=0

# Loop until the emulator boots up or the wait timeout is reached:
echo "Waiting for emulator to start:"

until [[ "${bootAnimationState}" =~ "${BOOT_ANIMATION_STOPPED}" ]]; do
  bootAnimationState=`adb -e shell getprop init.svc.bootanim 2>&1 &`

  if [[ "${bootAnimationState}" =~ "${BOOT_ANIMATION_NOT_FOUND}"
  || "$bootAnimationState" =~ "${BOOT_ANIMATION_OFFLINE}"
  || "${bootAnimationState}" =~ "${BOOT_ANIMATION_RUNNING}" ]]; then
    (( waitDuration += ${NEXT_STATE_CHECK_DELAY} ))
    # fixme: -en with '\r' does not replaces previous line ...
    #echo -en "\r$(formatWaitDuration ${waitDuration}) ..."
    echo "$(formatWaitDuration ${waitDuration}) ..."
    
    if [[ ${waitDuration} -gt ${TIMEOUT} ]]; then
      echo "Timeout (${TIMEOUT} seconds) reached; failed to start emulator!"
      exit 1
    fi
  fi
    # Wait for x seconds until next check.
    sleep ${NEXT_STATE_CHECK_DELAY}
done

echo "Emulator with name '${emulatorName}' successfully started."
